<?php

/**
 * Send a JSON API HTTP response to the client
 * @param array $response Payload to JSON-encode
 * @param int $status HTTP Status Code of the response
 */
function api_response(array $response, int $status = 200): string {
    header('Content-Type: application/json', True, $status);
    $json = json_encode($response);
    echo $json;
    return $json;
}


/**
 * Convert database result arrays to JSONifiable arrays
 * @param string $classname Name of the class to use for conversion
 * @param PDOStatement $db_result Database result handle to fetch results from
 * @return array JSON-ready array representation of the class
 */
function db_result_to_class(string $classname, PDOStatement $db_result): array {
    $db_col = $db_result->fetchAll();
    $class_col = array_map([$classname, "fromDb"], $db_col);
    $class_array = array_map([$classname, "toArray"], $class_col);

    return $class_array;
}
