<?php

/**
 * Flight between cities
 */
class flight {

    /**
     *
     * @var string Unique flight ID
     */
    protected $id;

    /**
     *
     * @var int UNIX timestamp of the time at which the plane takes off.
     */
    protected $time;

    /**
     *
     * @var string Unique ID of the trip this flight is for.
     */
    protected $tripId;


    /**
     * Basic Flight constructor which assigns a random time to the flight.
     * The tripWithCustomTime function should be used for custom times.
     * @param string $tripId
     */
    function __construct(string $tripId) {
        // In production, a UUID library with proper UUIDs should be used.
        $this->id = uniqid();

        $this->tripId = $tripId;

        // This is just a valid-but-fake UNIX timestamp
        $this->time = rand(time(), time() + 3600 * 24 * 64);
    }

    /**
     * Get Flight's time
     * @return int UNIX Timestamp
     */
    function getTime(): int {
        return $this->time;
    }

    /**
     * Get Flight's ID
     * @return string uniqid
     */
    function getId(): string {
        return $this->id;
    }

    /**
     * Get Flight's Trip's ID
     * @return string uniqid
     */
    function getTripId(): string {
        return $this->tripId;
    }

    /**
     * Return a JSON item representation of the Flight
     * @param flight $flight
     * @return string JSON string
     */
    static function toJson(flight $flight): string {
        return json_encode(flight::toArray($flight));
    }

    /**
     * Return an array representation of the Flight
     * @param flight $flight
     * @return array Associative array of the Flight's properties
     */
    static function toArray(flight $flight): array {
        $f["id"] = $flight->getId();
        $f["tripId"] = $flight->getTripId();
        $f["time"] = $flight->getTime();

        return $f;
    }

    /**
     * Recreate a Flight from its DB array representation
     * @param array $db_flight Database array result for a flight
     * @return \flight Instantiated Flight object
     */
    static function fromDb(array $db_flight): flight {
        $id = $db_flight[0];
        $trip_id = $db_flight[1];
        $time = $db_flight[2];

        $flight = new flight($trip_id);

        $flight->id = $id;
        $flight->time = $time;

        return $flight;
    }
}
