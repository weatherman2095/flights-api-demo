<?php

class trip {

    /**
     * @var string Unique trip ID
     */
    protected $id;

    /**
     * @var string Destination Airport of the Trip
     */
    protected $destination;

    /**
     *
     * @var string Source Airport of the Trip
     */
    protected $source;

    function __construct(string $source, string $destination) {
        $this->destination = $destination;
        $this->source = $source;

        // In production, a UUID library with proper UUIDs should be used.
        $this->id = uniqid();
    }

    /**
     * Get Trip's ID
     * @return string uniqid
     */
    function getId(): string {
        return $this->id;
    }

    /**
     * Get Trip's destination Airport
     * @return string
     */
    function getDst(): string {
        return $this->destination;
    }

    /**
     * Get Trip's source Airport
     * @return string
     */
    function getSrc(): string {
        return $this->source;
    }

    /**
     * Return a JSON item representation of the Trip
     * @param trip $trip
     * @return string JSON string
     */
    static function toJson(trip $trip): string {
        return json_encode(trip::toArray($trip));
    }

     /**
     * Return an array representation of the Trip
     * @param trip $trip
     * @return array Associative array of the Trip's properties
     */
    static function toArray(trip $trip): array {
        $t["id"] = $trip->getId();
        $t["dst"] = $trip->getDst();
        $t["src"] = $trip->getSrc();

        return $t;
    }

    /**
     * Recreate a Trip from its DB array representation
     * @param array $db_trip Database array result for a Trip
     * @return \trip Instantiated Trip object
     */
    static function fromDb(array $db_trip): trip {
        $id = $db_trip[0];
        $src = $db_trip[1];
        $dst = $db_trip[2];

        $trip = new trip($src, $dst);

        $trip->id = $id;
        return $trip;
    }

}
