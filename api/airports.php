<?php

include_once '../config/config.php';
include_once '../utils/utils.php';

/**
 * Get all airport names ordered alphabetically
 * @global type $airports
 * @return array Alphabetically sorted strings
 */
function listAirportsAlphabetically(): array {
    global $airports;
    $a = $airports;
    sort($a, SORT_ASC | SORT_STRING);

    return $a;
}

api_response(listAirportsAlphabetically());