<?php

include_once '../models/flight.php';
include_once '../data/db.php';
include_once '../utils/utils.php';


/**
 * Get all flights from the database for a Flight-only JSON dump
 * @return array All DB flights in Array representation
 */
function getAllFlights(): array {
    $db = getDbPDO();
    $db_res = $db->query("select * from flights");
    
    // We could query partners here too.

    return db_result_to_class("flight", $db_res);
}

api_response(getAllFlights());