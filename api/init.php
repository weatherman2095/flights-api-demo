<?php

include_once '../data/db.php';
include_once '../config/config.php';
include_once '../models/trip.php';
include_once '../models/flight.php';
include_once '../utils/utils.php';

/**
 * Deletion helper for use on test live versions if the db breaks
 * @global string $db_path
 */
function purge_db() {
    global $db_path;
    $status = unlink($db_path);

    $db_purge_ok = "Database deleted at path: " . $db_path;
    $db_purge_fail = "Database purge failed. File may not exist.";
    api_response([
        "status" => $status ? "ok" : "error",
        "info" => $status ? $db_purge_ok : $db_purge_fail
            ], $status ? 200 : 500);
}

/**
 * Initialization function for the program's database
 */
function initialize_db() {
    global $db_path;

    $db = getDbPDO(True);
    $create_flights = "CREATE TABLE IF NOT EXISTS flights "
            . "(id TEXT PRIMARY KEY,tripId TEXT, time INTEGER)";
    $create_trips = "CREATE TABLE IF NOT EXISTS trips "
            . "(id TEXT PRIMARY KEY, source TEXT, destination TEXT)";


    if ($db->exec($create_flights) === False) {
        api_response([
            "status" => "error",
            "info" => "Database error on Flights table creation."
                ], 500);
        return;
    }

    if ($db->exec($create_trips) === False) {
        api_response([
            "status" => "error",
            "info" => "Database error on Trips table creation."
                ], 500);
        return;
    }

    $db->beginTransaction();

    $trips = generate_trips();
    insert_trips($trips, $db);
    // Generate flights for places. For demo purposes,randomly generate twice
    // the number of trips.
    $flights = generate_flights($trips);
    insert_flights($flights, $db);

    $db->commit();

    $db_gen_msg = "Database generated at path: " . $db_path;
    api_response([
        "status" => "ok",
        "info" => $db_gen_msg
    ]);
}

/**
 * Database insertion helper function
 * @param array $flights Array-format flights to insert
 * @param PDO $db Database handler for insertion
 */
function insert_flights(array $flights, PDO $db) {
    $stmt = $db->prepare("INSERT INTO flights (id, tripId, time) "
            . "VALUES (:id, :tripId, :time)");

    foreach ($flights as $flight) {
        $stmt->execute([
            ":id" => $flight->getId(),
            ":tripId" => $flight->getTripId(),
            ":time" => $flight->getTime()
        ]);
    }
}

/**
 * Flight generation helper function
 * @param array $trips Array of Trip objects for DB relation IDs
 * @return array Array of Flight objects
 */
function generate_flights(array $trips): array {
    $num_trips = sizeof($trips);
    $max_flights = 2 * $num_trips;
    $flights = [];
    for ($i = 0; $i < $max_flights; $i++) {
        $j = rand(0, $num_trips - 1); // True randomness isn't needed.
        $flights[] = new flight($trips[$j]->getId());
    }

    return $flights;
}

/**
 * Insert all trips in the array into the Database
 * @param array $trips
 * @param PDO $db
 */
function insert_trips(array $trips, PDO $db) {
    $stmt = $db->prepare("INSERT INTO trips (id, source, destination)"
            . " VALUES (:id, :src, :dst)");
    foreach ($trips as $trip) {
        $stmt->execute([
            ":id" => $trip->getId(),
            ":src" => $trip->getSrc(),
            ":dst" => $trip->getDst()
        ]);
    }
}

/**
 * Generate all non self-directed airport pairs
 * @global array $airports
 * @return array
 */
function generate_trips(): array {
    global $airports;
    $trips = [];

    foreach ($airports as $src) {
        foreach ($airports as $dst) {
            if (strcmp($src, $dst) != 0) {
                $trips[] = new trip($src, $dst);
            }
        }
    }

    return $trips;
}

/**
 * Debug function to display a JSON dump of the database contents
 */
function print_db_dump() {
    $db = getDbPDO();
    $flights_obj = $db->query("select * from flights");
    $db_flights = $flights_obj->fetchAll();
    $trips_obj = $db->query("select * from trips");
    $db_trips = $trips_obj->fetchAll();

    $trips = array_map(["trip", "fromDb"], $db_trips);
    $array_trips = array_map(["trip", "toArray"], $trips);

    $flights = array_map(["flight", "fromDb"], $db_flights);
    $array_flights = array_map(["flight", "toArray"], $flights);

    api_response([
        "trips" => $array_trips,
        "flights" => $array_flights
    ]);
}

$selector = filter_input(INPUT_GET, "action");
if ($selector == "init") {
    initialize_db();
} elseif ($selector == "purge") {
    purge_db();
} elseif ($selector == "dump") {
    print_db_dump();
}