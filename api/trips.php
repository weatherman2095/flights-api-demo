<?php

include_once '../data/db.php';
include_once '../models/trip.php';
include_once '../models/flight.php';
include_once '../utils/utils.php';

/**
 * Print JSON listing of all possible trips, which can then be individually
 * queried for available flights.
 */
function listAllTrips() {
    $db = getDbPDO();
    $stmt = $db->query("select * from trips");

    // Here we could query partner databases and enrich our results. This should
    // be ideally cached to prevent hammering servers. Frequency of cache upates
    // would have to be evaluated on reasonable change expectations.

    $array_trips = db_result_to_class("trip", $stmt);

    api_response($array_trips);
}

/**
 * Print a JSON listing of all available Flights for a specific Trip
 * @param string $tripId Unique Trip ID
 */
function getTripFlights(string $tripId) {
    $db = getDbPDO();
    $stmt = $db->prepare('select * from flights where tripId = :tripid');
    $stmt->bindValue(":tripid", $tripId);
    $stmt->execute();

    // Here we could also fetch similar queries to our partners. Standardized
    // transit codes for trips & airports would be useful in reducing
    // unnecessary conversion code...

    $array_flights = db_result_to_class("flight", $stmt);

    api_response($array_flights);
}

/**
 * Main Flight insertion controller
 * Mostly does query sanity verification
 * @param string $json_flight JSON formatted Flight to add to the DB
 */
function add_flight(string $json_flight) {
    // A more efficient function would verify, accept and insert arrays of
    // flights.
    $json = json_decode($json_flight, true);
    $db = getDbPDO();
    $tripId = $json["tripId"];
    $stmt1 = $db->prepare("select count(*) from trips where id = '?'");
    $stmt1->execute($tripId);
    $res = $stmt1->fetchAll();
    if ($res < 1) {
        api_response([
            "status" => "error",
            "info" => "No trips with id: " . $tripId . " exist."
                ], 404);
        return;
    }


    $time = $json["time"];
    if ($time == NULL) {
        api_response([
            "status" => "error",
            "info" => "No time given for flight."
                ], 403);
        return;
    }

    insert_flight_to_db($tripId, $time, $db);
}

/**
 * Flight DB insertion helper
 * @param string $tripId Trip associated with the Flight
 * @param int $time Pseudotime at which the Flight occurs
 * @param PDO $db Database Handler
 */
function insert_flight_to_db(string $tripId, int $time, PDO $db) {
    $id = uniqid();
    // If we were to loop and add an array, transactions are more efficient.
    $db->beginTransaction();
    $stmt2 = $db->prepare('INSERT INTO flights (id, tripId, time) '
            . 'VALUES (:id, :tripId, :time)');
    $stmt2->execute([
        ":id" => $id,
        ":tripId" => $tripId,
        ":time" => $time
    ]);
    if (!$db->commit()) {
        api_response([
            "status" => "error",
            "info" => "Value insert failed due to unknown error."
                ], 500);
    } else {
        api_response([
            "status" => "ok",
            "info" => [
                "id" => $id,
                "tripId" => $tripId,
                "time" => $time
            ]
        ]);
    }
}

/**
 * Flight DB deletion
 * @param string $id Flight to delete
 */
function delete_flight(string $id) {
    $db = getDbPDO();
    $stmt = $db->prepare("DELETE FROM flights WHERE id = :id");
    $stmt->execute([":id" => $id]);
    $rows = $stmt->rowCount();
    if ($rows < 1) {
        api_response([
            "status" => "error",
            "info" => "Flight could not be deleted.",
            "more" => $stmt->errorInfo()
                ], 500);
    } else {
        api_response([
            "status" => "ok",
            "info" => "Flight was successfully deleted."
        ]);
    }
}

/**
 * GET /api/trips.php processing dispatcher
 */
function manage_get() {
    $action = filter_input(INPUT_GET, "action");
    $id = filter_input(INPUT_GET, "id") ?? NULL;
    if ($action == "all") {
        listAllTrips();
    } elseif ($action == "flights") {
        if ($id == NULL) {
            api_response([
                "status" => "error",
                "info" => "A trip ID is required to list its flights."
                    ], 500);
        }

        getTripFlights($id);
    } else {
        api_response([
            "status" => "error",
            "info" => "No appropriate action in query"
        ], 404);
    }
}

/**
 * POST /api/trips.php  processing dispatcher
 */
function manage_post() {
    $json = json_decode(file_get_contents('php://input'), true);
    $action = $json["action"] ?? NULL;
    if ($action == "addflight") {
        $tripId = $json["tripId"];
        $time = $json["time"];

        $json = json_encode([
            "tripId" => $tripId,
            "time" => (int) $time
        ]);
        add_flight($json);
    } elseif ($action == "delflight") {
        $id = $json["id"];
        delete_flight($id);
    } else {
        api_response([
            "status" => "error",
            "info" => "No appropriate action in query"
        ], 404);
    }
}

$reqtype = filter_input(INPUT_SERVER, "REQUEST_METHOD");
if (strcmp($reqtype, "GET") == 0) {
    manage_get();
} elseif (strcmp($reqtype, "POST") == 0) {
    manage_post();
}