<?php

include_once '../config/config.php';
include_once '../utils/utils.php';

/**
 * Obtain Database PDO handle object
 * @global string $db_path Path to the Database File
 * @global string $db_uri PDO-compatible sqlite uri
 * @param bool $initialize True if initializing the DB, else false or nothing
 * @return PDO
 */
function getDbPDO(bool $initialize = False): PDO {
    global $db_path;    
    global $db_uri;
    
    // If initializing, it is normal for it not to exist.
    if (!$initialize && !file_exists($db_path)){
        api_response([
            "status" => "error",
            "info" => "No database file exists, run initialization first."
        ], 503);
        die();
    }
    
    try {
        return new PDO($db_uri);
    } catch (PDOException $e) {
        api_response([
            "status" => "error",
            "info" => "Error!: " . $e->getMessage()
        ], 503);
        die();
    }
}
