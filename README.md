# Flights API

This is a simple toy program to demonstrate PHP interaction with
HTTP/JSON and the PDO database interface.

In this case, the PDO backend used is the SQLite version.

## Setup

### Manual Installation

Installation instructions will be based on a Debian 10 server's setup.

- Install `php7.3`, `sqlite3`, `php-sqlite3`, `nginx-full` and `php-fpm`.
- Extract the project tarball in your intended `nginx` site directory.
- Edit the database path `$db_path` in `config/config.php` to a path
  of your choice where `www-data` has read and write permissions. (On
  the demo-server, that's `/var/www/data/sqlite.db`.)
- Create an `nginx` configuration for the directory you chose and
  enable fastcgi processing of files with the socket interface. (The
  default example in `/etc/nginx/sites-available/default` is good. You
  can modify that file.)

Note: Setting permissions can be via directory ownership, setting file
ACL, etc.

### Application Initialization

- Run `http://<my-server-host>/api/init.php?action=init`
- Optional: To reinitialize the database fully (if one already
  exists), run `http://<my-server-host>/api/init.php?action=purge` and
  rerun step 1.
- To verify the initialization ran successfully, run
  `http://<my-server-host>/api/init.php?action=dump`

### Automatic Installation - Using the provided ansible playbook

- To run the ansible file, create a `~/.ansible.cfg` file and designate an
  inventory file.
- In the inventory file, add your server as documented [here](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#inventory-basics-formats-hosts-and-groups).
- Modify the ansible playbook `hosts:` line to instead reference your
  server(s) or the relevant group.
- Ensure either a user called `bob` with sudoer rights on the server
  exists, or modify the playbook's `remote_user` and put the name of
  the user you want.
- run `ansible-playbook -C -K -D <my-playbook-file>`
- If everything looks okay, rerun the file without the `-C`.
- Go through `Application Initialization` steps.

#### Directory Structure

Originally, this project was packaged mimicking Ansible role
structure:
- flights-demo/
- flights-demo/server-playbook.yml
- flights-demo/files/
- flights-demo/files/htpasswd
- flights-demo/files/nginx-401-filter.conf
- flights-demo/files/nginx-401-jail.conf
- flights-demo/files/nginx-php-config
- flights-demo/files/nginx-php-config-auth
- flights-demo/files/project.tar.gz

Where project.tar.gz is the output of running `git archive
--format=tar.gz master > project.tar.gz` on this repository.

#### Bugs and Errors

It is possible that `ansible` fails on `lineinfile`'s check for the
firewalld config to fix. This is a bug, probably related to
[#9546](https://github.com/ansible/ansible/issues/9546), although I'm
not sure.

Make sure that either your `ansible` configuration sets the
`ansible_python_interpreter` to `/usr/bin/python3` **or** when
invoking `ansible-playbook` use `-e
'ansible_python_interpreter=/usr/bin/python3'`. This is required
because Debian 10 only install the Python3 module of `firewalld`.

## Entities
### Flight

- id: string => Unique ID
- tripId: string => Unique ID of the Flight's Trip
- time: int => Fake UNIX timestamp of the Flight departure time.

### Trip

- id: string => Unique ID
- source: string => Source Airport of the Trip
- destination: string => Destination Airport of the Trip

## API documentation

### Initialization API (debugging) - `api/init.php`

- GET `api/init.php?action=init`: Initializes the database tables and fills them
  with randomly generated flights for trips generated from a static
  list of airports.
- GET `api/init.php?action=purge`: Delete the database file from disk.
- GET `api/init.php?action=dump`: Dump the entire contents of the database
  into a JSON response.

Due to serving primarily as a debugging interface which should *not*
be available in production, invalid action calls simply return
nothing.

Trivia: Airport names were picked from Wikipedia's lists of busiest
airports in 2018/2019.

### Airports API endpoint

- GET `api/airports.php`: Return an alphabetically ordered JSON
  listing of all Airports in the static list in `config/config.php`.

No errors will occur from adding garbage to the query, as this
endpoint only provides the described output.

### Flights API endpoint (debugging)

- GET `api/flights.php`: Return a JSON listing of all existing Flights.

No errors will occur from adding garbage to the query, as this
endpoint only provides the described output.

### Trips API endpoint

- GET `api/trips.php?action=all`: Return a listing of all Trips in the
  database.
- GET `api/trips.php?action=flights&id=<tripId>`: Return a listing of all
  Flights for a specific Trip referenced by its ID (tripId).

Errors will occur on trying to use invalid `action` values in this endpoint.

#### Trips API POST functions

POST functions are referenced by their `action` value.

##### addflight

```json
{
    "action": "addflight",
	"tripId": "<tripId>",
	"time": time
}
```

- action: string
- tripId: string
- time: int (pseudo UNIX timestamp)

This endpoint adds a single flight to a specific Trip referenced by
its tripId. Invalid IDs will result in an API error value.

##### delflight

```json
{
    "action": "delflight",
	"ip": "id"
}
```

- action: string
- id: string

This endpoint deletes a single flight referenced by its "unique" ID (`uniqid()`).

## Notes from the author

### Server Basic-Auth

The demo server will have basic-auth enabled for security's sake.

A proper internal server wouldn't require this granted it has proper security
monitoring or runs exclusively within a VPN.

The required credentials will be provided by email.

### Querying Partners

Depending on time constraints, it might make more sense to
periodically query partners via `cron` to add values to the database
from another script rather than querying partners during active user
interaction.

Visible delays on the partner-side might cause user-frustration.

### readme.html

That file was generated with:

```
pandoc 2.2.1
Compiled with pandoc-types 1.17.5.1, texmath 0.11.1, skylighting 0.7.5
Default user data directory: ~/.pandoc
Copyright (C) 2006-2018 John MacFarlane
Web:  http://pandoc.org
This is free software; see the source for copying conditions.
There is no warranty, not even for merchantability or fitness
for a particular purpose.
```

### Note regarding the class->database mapping of values

If one uses an actual ORM library/framework, manually defining and linking
between unique IDs and collections would be unnecessary.
