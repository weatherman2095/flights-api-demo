<?php

/**
 * @var string Database Path for initialize-deletion if needed
 */
$db_path = getenv("HOME") . "/flights-api/data/" . "testdb.sqlite";

/**
 * @var string Database URI for the Application's PDO interaction
 */
$db_uri = "sqlite:" . $db_path;

/**
 * @var array Airport names taken from Wikipedia
 */
$airports = [
    "Toronto Pearson International Airport",
    "Vancouver International Airport",
    "Montréal-Trudeau International Airport",
    "Calgary International Airport",
    "Ottawa Macdonald–Cartier International Airport",
    "Kelowna International Airport",
    "St. John's International Airport",
    "Sheremetyevo International Airport",
    "Domodedovo International Airport",
    "Vnukovo International Airport",
    "Pulkovo Airport",
    "Sochi International Airport",
    "Koltsovo Airport",
    "Tolmachevo Airport",
    "Paris Orly",
    "Nice",
    "Lyon Saint Exupéry",
    "Toulouse Blagnac",
    "Bâle Mulhouse",
    "Marseille"
];
